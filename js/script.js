function ageZodiac(userEnter) {
  let zodiac = new Map([
    ["01/19", "Kozerog"],
    ["02/18", "Vodoley"],
    ["03/20", "Fish"],
    ["04/19", "Oven"],
    ["05/20", "Telec"],
    ["06/20", "Twins"],
    ["07/22", "Rak"],
    ["08/22", "Leo"],
    ["09/22", "Deva"],
    ["10/22", "Vesi"],
    ["11/21", "Scorpio"],
    ["12/21", "Strelec"],
    ["12/31", "Kozerog"]
  ])

  let year = ["Rat", "Ox", "Tiger", "Rabbit", "Dragon", "Snake", "Horse", "Goat", "Monkey", "Rooster", "Dog", "Pig"];

  let now = new Date();
  let buffer = userEnter.split(".");
  let userDate = buffer[1] + "/" + buffer[0] + "/" + buffer[2];
  let user = new Date(userDate);
  let time= now - user;

  let sec = time / 1000;
  let mins = sec / 60;
  let hours = mins / 60;
  let days = hours / 24;
  let years = Math.floor(days / 365.25);
  let ageResult = years;

  let userShortDate = userDate.slice(0,5);

  for(let date of zodiac.keys()) {
    if (new Date(userShortDate) <= new Date(date)) {
      var zodiacResult = zodiac.get(date);
      break;
    }
  }

  let animalNumber = (+buffer[2] - 1888) % 12;
  let animal = year[animalNumber];


  return `You are ${ageResult} years old. Your zodiac is ${zodiacResult}.
  Your animal in Chinees calendar is ${animal}.`
}


let entry = prompt("Enter your full birth date: ");
alert(ageZodiac(entry));
